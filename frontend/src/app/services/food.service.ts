import { Injectable } from '@angular/core';
import { sampleFoods, sampleTags } from '../../data';
import { Food } from '../shared/models/food';
import { Tag } from '../shared/models/tag';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor() {
    this.getAll()
  }

  getAll(): Food[] {
    return sampleFoods
  }

  getFoodBySearch(seachTerm: string) {
    return this.getAll().filter(food => food.name.toLowerCase().includes(seachTerm.toLowerCase()))
  }

  getFoodById(foodId: string): Food {
    return this.getAll().find(food => food.id === foodId) ?? new Food
  }

  getAllTags(): Tag[] {
    return sampleTags
  }

  getAllFoodsByTag(tag: string): Food[] {
    return tag === "All" ?
      this.getAll() :
      this.getAll().filter(food => food.tags?.includes(tag))
  }
}
